#!/bin/bash

set -e

base=$(dirname $0)
if [ "$base" == '.' ]; then
    base="$PWD"
fi

# import_mbox
# List archive import
# from Unix mbox format
# to Sympa's Web Archive (MHonArc)
#
# Based on Sympa contribs and tools
# Not too much sexy, as it won't be useful after
# initial conversion (during Sympa setup phase)
#
# Needs a mbox file and a Sympa listname
# Copies (just in case ;-) the mbox file
# under the archives directory of the list
#
# Written by E.Eyer - ESRF - 25 Oct 06
# Mostly rewritten by Hendrik - 11 Aug 14

# Configuration variables
SYMPA_USER=sympa:sympa
SYMPA_ROOT=/var/lib/sympa
MBOX_BACKUP=import.mbox

set -e

# Syntax check
SYNTAX="$0 archive.mbox listname domain"
MBOX_FILE="$1"
LIST_NAME="$2"
SYMPA_DOMAIN="$3"
if [ ! -f "$MBOX_FILE" ]; then
    echo "Missing or invalid archive file!" >&2
    echo "$SYNTAX" >&2
    exit 1
fi
if [ ! -d $SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME ]; then
    echo "Missing or invalid list name!" >&2
    echo "$SYNTAX" >&2
    exit 1
fi
arcdir=$SYMPA_ROOT/arc/$LIST_NAME@$SYMPA_DOMAIN
if [ -e $arcdir ]; then
    echo "ERROR: arc dir '$arcdir' already exists, please clean up first" >&2
    exit 1
fi

# Create archives directory if not yet done
mkdir -p "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives"

# Check directory is empty (prevents dialogs...)
COUNT=$(ls $SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives | wc -l)
if [ $COUNT -gt 0 ]; then
    echo "Sorry, Sympa text archives directory not empty - I stop!" >&2
    exit 2
fi

# Copy mbox file
cp -v "$MBOX_FILE" "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives/$MBOX_BACKUP"
pushd "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives"

# Convert to Sympa text archives
$base/mbox2sympa.pl $MBOX_BACKUP
rm -f $MBOX_BACKUP

chown -R $SYMPA_USER "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives"
chmod -R g=rX,o= "$SYMPA_ROOT/list_data/$SYMPA_DOMAIN/$LIST_NAME/archives"

# Convert to Sympa web archives input files
function arc2webarc_filter() {
    grep -vxE ' *|Bursting archives|Found [0-9]* messages|Rebuilding HTML|notice Sympa::Spool::store\(\) Sympa::Message <> is stored into Sympa::Spool::Archive as <.*>|Have a look in /var/lib/sympa/arc/.*/-/ directory for messages dateless|Now, you should add a web_archive parameter in the config file to make it accessible from the web' >&2 || true
}
if ! sudo -u sympa /usr/share/sympa/bin/arc2webarc.pl $LIST_NAME $SYMPA_DOMAIN 2> >(arc2webarc_filter); then
    echo "Failed: sudo -u sympa /usr/share/sympa/bin/arc2webarc.pl $LIST_NAME $SYMPA_DOMAIN" >&2
    exit 1
fi

# That's it!
exit 0
