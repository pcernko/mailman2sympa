This project was cloned from:
https://gitlab.fechner.net/mfechner/mailman2sympa.git

mailman-subscriber3.py from
https://www.msapiro.net/scripts/mailman-subscribers3.py see [How do I
extract (export) a list of my list's members
(subscribers)?](https://wiki.list.org/DOC/How%20do%20I%20extract%20%28export%29%20a%20list%20of%20my%20list%27s%20members%20%28subscribers%29%3F).

Initially cloned from: https://git.fs.lmu.de/roots/mailman2sympa

# Mailman2 to Sympa migration

This setup is used to migrate mailman to sympa.

## Preparation

Copy the `mailman2sympa` dir to your sympa's `create_list_templates`
directory. This template is used for creating the Sympa list with all
migrated settings from the Mailman2 list.

## Migration

Execute for each mailinglist the command:
```sh
export MMPW='<your mailman2 list admin password>'
mm2sympa.sh LIST@lists.DOMAIN
```

This will migrate the Mailman2 list `LIST@lists.DOMAIN` to Sympa as
`LIST@sympa.DOMAIN` (note that `lists` will be replaced with `sympa`
in the domain).

If you want to have the new list with a different name (or domain) you
can specify the new name as optional second parameter:
```sh
mm2sympa.sh MMLIST@MMDOMAIN SYMPALIST@SYMPADOMAIN
```

# Notes

For full migration support, add the [whitelist/modlist
add-in](https://github.com/sshipway/sympa-6.2-plugins/tree/master/whitelist-1.2). This
will allow migration of whitelisting and user specific moderation
flags.
