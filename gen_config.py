#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

import argparse
import xml.etree.ElementTree as ET
import json

def err(msg):
    print('ERROR: '+msg, file=sys.stderr)
def warn(msg):
    print('WARNING: '+msg, file=sys.stderr)
def info(msg):
    print(msg)

try:
    import mpildap
    mpildap_available = True
    own_domains = set(d for o in mpildap.ldaps('istMailDomainReceive=*', 'istMailDomainReceive') for d in o)
except ModuleNotFoundError:
    warn('mpildap module not available, email normalization will be skipped')
    mpildap_available = False
    pass

def normalize_email(email, required=False):
    if not mpildap_available:
        return email
    lp, domain = email.split('@')
    ldap_mail = mpildap.ldaps(f'&(objectClass=istMailAccount)(istEmailName={lp})(istMailDomainReceive={domain})', ['mail', 'istIsPseudoAccount'], unique=True)
    if ldap_mail:
        if ldap_mail['istIsPseudoAccount']:
            err(f'Address {email} belongs to a pseudo account')
            exit(1)
        return ldap_mail['mail'][0]
    if required and domain in own_domains:
        err(f'Address {email} in own domains does not exist or is a MailGroup!')
        exit(1)
    return email

regexp_mapping = {}
def unregexp(e, listtype):
    global regexp_mapping
    if e in regexp_mapping:
        info(f"mapping {listtype} entry '{e}' to '{regexp_mapping[e]}'")
        return regexp_mapping[e]
    for char in '*^$':
        if char in e:
            warn(f"{listtype} entry '{e}' contains regexp char '{char}', this won't work!")
            break
    return e
        

parser = argparse.ArgumentParser(description='import mailman list to sympa')
parser.add_argument('--regexp-mapping', type=str, help='json data mapping MM regexp to Sympa glob patterns', default=None)
parser.add_argument('input')
parser.add_argument('membersin')
parser.add_argument('output')
parser.add_argument('info')
parser.add_argument('blocklist')
parser.add_argument('whitelist')
parser.add_argument('modlist')
parser.add_argument('membersout')
args = parser.parse_args()

if args.regexp_mapping:
    with open(args.regexp_mapping) as fd:
        regexp_mapping = json.load(fd)

members = {}
with open(args.membersin) as fd:
    for m, d in json.load(fd).items():
        members[normalize_email(m)] = d

old_vars = {}
with open(args.input) as fd:
    exec(fd.read(), old_vars)

defaults = {
    'msg_footer': """_______________________________________________
%(real_name)s mailing list
%(real_name)s@%(host_name)s
%(web_page_url)slistinfo%(cgiext)s/%(_internal_name)s""",
}
# sign_policy & encrypt_policy are from https://people.mpi-klsb.mpg.de/~pcernko/mailman-pgpsmime.shtml
for k in 'umbrella_list topics emergency personalize member_moderation_action welcome_msg msg_header msg_footer sign_policy encrypt_policy'.split():
    if not old_vars.get(k):
        continue
    value = old_vars[k]
    if defaults.get(k) == value:
        continue
    if isinstance(value, str):
        value = value.replace('\n', '\\n')
    warn(f'"{k}" is set (or non-default), automatic migration not support, please fix manually: "{value}"')


xml = ET.Element("list")

ET.SubElement(xml, "listname").text = old_vars['new_listname']
ET.SubElement(xml, "status").text = 'open'
ET.SubElement(xml, "topic").text = 'other'
ET.SubElement(xml, "type").text = 'mailman2sympa'

# subject is required in sympa
if old_vars['description']:
    ET.SubElement(xml, "subject").text = old_vars['description']
else:
    ET.SubElement(xml, "subject").text = old_vars['real_name']

ET.SubElement(xml, "custom_subject").text = old_vars['subject_prefix'].strip().replace('[', '').replace(']', '')

if old_vars['advertised']:
    ET.SubElement(xml, "visibility").text = 'noconceal'
    ET.SubElement(xml, "info").text = 'open'
else:
    ET.SubElement(xml, "visibility").text = 'conceal'
    ET.SubElement(xml, "info").text = 'conceal'

if old_vars['subscribe_policy'] == 0:
    ET.SubElement(xml, "subscribe").text = 'owner'
elif old_vars['subscribe_policy'] == 1:
    ET.SubElement(xml, "subscribe").text = 'auth_notify'
else:
    ET.SubElement(xml, "subscribe").text = 'auth_owner'

if old_vars['unsubscribe_policy']:
    ET.SubElement(xml, "unsubscribe").text = 'owner'
else:
    ET.SubElement(xml, "unsubscribe").text = 'open_notify'

if old_vars['archive']:
    ET.SubElement(xml, "process_archive").text = 'on'
    a = ET.SubElement(xml, "archive")
    if old_vars['archive_private']:
        ET.SubElement(a, "web_access").text  = 'private'
        ET.SubElement(a, "mail_access").text = 'private'
    else:
        ET.SubElement(a, "web_access").text  = 'public'
        ET.SubElement(a, "mail_access").text = 'public'
else:
    ET.SubElement(xml, "process_archive").text = 'off'

r = ET.SubElement(xml, "reply_to_header")
if old_vars['reply_goes_to_list'] == 0:
    ET.SubElement(r, 'value').text = 'sender'
elif old_vars['reply_goes_to_list'] == 1:
    ET.SubElement(r, 'value').text = 'list'
elif old_vars['reply_goes_to_list'] == 2:
    if old_vars['reply_to_address'] == old_vars['new_listname'] + '@' + old_vars['robot']:
        ET.SubElement(r, 'value').text = 'list'
    else:
        ET.SubElement(xml, 'value').text = 'other_email'
        ET.SubElement(xml, 'other_email').text = old_vars['reply_to_address']
if old_vars['first_strip_reply_to']:
    ET.SubElement(r, 'apply').text = 'forced'
else:
    ET.SubElement(r, 'apply').text = 'respect'

if old_vars['private_roster'] == 0:
    ET.SubElement(xml, 'review').text = 'public'
elif old_vars['private_roster'] == 1:
    ET.SubElement(xml, 'review').text = 'private'
else:
    ET.SubElement(xml, 'review').text = 'owner'

if old_vars['anonymous_list']:
    ET.SubElement(xml, 'anonymous_sender').text = old_vars['new_listname'] + '@' + old_vars['robot']

# send
moderated = False
if old_vars['generic_nonmember_action'] == 0: # accept
    ET.SubElement(xml, 'send').text = 'public'
elif old_vars['generic_nonmember_action'] >= 1: # hold, reject, discard
    if old_vars['default_member_moderation']:
        ET.SubElement(xml, 'send').text = 'editorkey'
        moderated = True
    else:
        ET.SubElement(xml, 'send').text = 'privateoreditorkey'

if old_vars['max_message_size'] == 0:
    # if unlimited is requested, we set 100MB (in kB) as sympa does not support unlimited
    old_vars['max_message_size'] = 100*1024
if old_vars['max_message_size']*1024 > 5242880:
    ET.SubElement(xml, 'max_size').text = str(old_vars['max_message_size']*1024) # sympa used bytes not kB

if old_vars['digestable']:
    ET.SubElement(xml, "digest").text = 'true'
else:
    ET.SubElement(xml, "digest").text = 'false'


# send_welcome_msg not used, already handled in 'subscribe'
# send_goodbye_msg not usable
# admin_immed_notify ignored, we should always notify immediately
# admin_notify_mchanges ignored, only used in d2-seminar
# respond_to_post_requests unsupported
# new_member_options unsupported
# administrivia not used
# nondigestable unsupported
# scrub_nondigest not used
# regular_exclude_lists not used
# regular_exclude_ignore not used
# regular_include_lists not used
# digest_is_default not used
# mime_is_default_digest not used
# digest_size_threshhold unsupported
# digest_send_periodic unsupported
# digest_volume_frequency unsupported
# obscure_addresses unsupported
# dmarc_moderation_action not used
# forward_auto_discards unsupported
# require_explicit_destination unsupported
# acceptable_aliases unsupported
# max_num_recipients unsupported
# header_filter_rules unsupported
# bounce_processing not used
# filter_mime_types not used
# pass_mime_types not used
# filter_filename_extensions not used
# from_is_list unsupported with dmarc
# umbrella_member_suffix not used
# send_reminders unsupported for specific lists
# goodbye_msg not used
# digest_header not used
# digest_footer not used

owners = list(set(normalize_email(e, required=True) for e in old_vars['owner']))
ET.SubElement(xml, "creation_email").text = owners[0]
for owner in owners:
    o = ET.SubElement(xml, "owner", attrib={'multiple':'1'})
    ET.SubElement(o, "email").text = owner

moderators = list(set(normalize_email(e, required=True) for e in old_vars['moderator']))
for mod in moderators:
    o = ET.SubElement(xml, "editor", attrib={'multiple':'1'})
    ET.SubElement(o, "email").text = normalize_email(mod)


tree = ET.ElementTree(xml)
try:
    tree.write(args.output, encoding='utf-8', xml_declaration=True)
except Exception as e:
    err(f'Failed to write XML for sympa: {e}')
    exit(1)

with open(args.info, mode='w') as fd:
    fd.write(old_vars['info'])

blocklist = []
for block in sorted(set(old_vars['ban_list'] + old_vars['hold_these_nonmembers'] + old_vars['reject_these_nonmembers'] + old_vars['discard_these_nonmembers'])):
    entry = unregexp(block, 'blocklist')
    if entry:
        blocklist.append(entry)
if blocklist:
    # ensure non-empty file ends with newline
    blocklist.append('')
with open(args.blocklist, mode='w') as fd:
    fd.write('\n'.join(blocklist))

whitelist = []
for white in sorted(set(old_vars['accept_these_nonmembers'])):
    entry = unregexp(white, 'whitelist')
    if entry:
        whitelist.append(entry)
if moderated:
    for m, d in members.items():
        if d['_mod'] == 'off':
            info(f'Whitelisting member "{m}" for list with moderated policy')
            whitelist.append(m)
if whitelist:
    # ensure non-empty file ends with newline
    whitelist.append('')
with open(args.whitelist, mode='w') as fd:
    fd.write('\n'.join(whitelist))

modlist = []
if not moderated:
    for m, d in members.items():
        if d['_mod'] == 'on':
            info(f'Mod-listing member "{m}" for list with open policy')
            modlist.append(m)
if modlist:
    # ensure non-empty file ends with newline
    modlist.append('')
with open(args.modlist, mode='w') as fd:
    fd.write('\n'.join(modlist))

with open(args.info, mode='w') as fd:
    fd.write(old_vars['info'])

with open(args.membersout, mode='w') as fd:
    for m, d in members.items():
        email = m
        gecos = d['_realname']
        reception = 'mail'
        if d['_nomail'] != "off":
            reception = 'nomail'
        elif d['_digest'] == 'on':
            if d['_plain'] == 'on':
                reception = 'digestplain'
            else:
                reception = 'digest'
        elif d['_notmetoo'] == 'on':
            reception = 'not_me'
        visibility = 'noconceal'
        if d['_hide'] == 'on':
            visibility = 'conceal'
        print(f"""email {email}
gecos {gecos}
reception {reception}
subscribed 1
visibility {visibility}
""", file=fd)
