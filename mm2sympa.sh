#!/bin/bash

base=$(dirname $0)

set -e

mailmanlist=$1
if [ -z "$mailmanlist" ]; then
    echo "Usage: $0 LIST@lists.DOMAIN" >&2
    exit 1
fi
mmlistname=${mailmanlist%@*}
mmdomain=${mailmanlist#*@}
sympalistname=$mmlistname
robot=${mmdomain/lists/sympa}
if [ -n "$2" ]; then
    sympalistname=${2%@*}
    robot=${2#*@}
fi
mmarchive=/var/lib/mailman/archives/private/$mmlistname.mbox/$mmlistname.mbox

if [ ! -f "/etc/sympa/$robot/robot.conf" ]; then
    echo "robot $robot does not exist" >&2
    exit 1
fi

if [ -z "$MMPW" ]; then
    echo "Please set MMPW to the listadmin password of $mmdomain!" >&2
    exit 1
fi

function sshml() {
    ssh -o ControlPath="~/.ssh/cp-$MPI_ROOT-%C" -o ControlMaster=auto -o ControlPersist=600s $mmdomain -l root "$@"
}

wwsympa_url=$(awk '$1=="wwsympa_url" { print $2; }' /etc/sympa/$robot/robot.conf)

tmpdir=$(mktemp -t -d mm2sympa.${mailmanlist/@/AT}.XXXXXXXX)
# sympa user needs access
chmod go+rx $tmpdir
trap "rm -rf $tmpdir" 0 1 2 5 15 EXIT

#set -x

# fetch members
membersjson=$tmpdir/members.json
$base/mailman-subscribers3.py \
       --url_path /admin \
       --ssl \
       --json \
       $mmdomain $mmlistname "$MMPW" \
       > $membersjson

# fetch mailman config
inputpy=$tmpdir/input.py
sshml config_list -o /dev/stdout $mmlistname > $inputpy
# make all strings unicode
sed -i -e "s/ '/ u'/" -e 's/ "/ u"/' $inputpy
# enhance config with additional variables for gen_config.py
cat <<EOF >> $inputpy
old_listname = u'$mmlistname'
new_listname = u'$sympalistname'
robot = u'$robot'
url = u'$wwsympa_url'
EOF

# generate sympa XML, info, blocklist, whitelist, modlist and member dump
configxml=$tmpdir/input.xml
infofile=$tmpdir/info
blocklist=$tmpdir/blocklist.txt
whitelist=$tmpdir/whitelist.txt
modlist=$tmpdir/modlist.txt
memberdump=$tmpdir/member.dump
regexp_mapping=$base/regexp_mapping.json
opts=()
if [ -e $regexp_mapping ]; then
    opts=( "${opts[@]}" --regexp-mapping $regexp_mapping )
fi
$base/gen_config.py "${opts[@]}" $inputpy $membersjson $configxml $infofile $blocklist $whitelist $modlist $memberdump
# sympa user needs access
chmod 0644 $configxml

# now generate list based on config XML
sympa --create_list --robot $robot --input_file $configxml 2> >(grep -vxF 'create_list [notice] Aliases have been installed. ()')

# load users from member.dump
listdir=/var/lib/sympa/list_data/$robot/$sympalistname
install -m 640 -o sympa -g sympa $memberdump $listdir
sympa --restore_users --list=$sympalistname@$robot 2> >(grep -vxF "$sympalistname@$robot: Restored list users (member)")

# install info
install -m 640 -o sympa -g sympa $infofile $listdir

# install blocklist, whitelist and modlist
search_filters=$listdir/search_filters
install -d -m 750 -o sympa -g sympa $search_filters
for f in  $blocklist $whitelist $modlist; do
    [ -s $f ] || continue
    install -m 640 -o sympa -g sympa $f $search_filters
done

# import archive
localarchive=$tmpdir/archive.mbox
if sshml test -r $mmarchive; then
    sshml cat $mmarchive > $localarchive
    chmod 644 $localarchive
    $base/import_mbox.sh $localarchive $sympalistname $robot > $tmpdir/archive.log
fi

# store migration temp data in list dir
tar=$listdir/mailman2sympa.tar
(cd $tmpdir && tar -cf $tar * && chown sympa: $tar)

if [ -t 1 ]; then
    cat <<EOF

Migration successful.
You can now remove the Mailman2 list with:

ssh root@$mmdomain rmlist -a $mmlistname

Optionally add the following redirect to your Mailman2 server's MTA:

$mmlistname@$mmdomain: $sympalistname@$robot

Optionally add these redirects to your Mailman2 server's Webserver:

RedirectMatch ^/(listinfo|options|roster)/$mmlistname https://$robot/wws/info/$sympalistname
RedirectMatch ^/private/$mmlistname                   https://$robot/wws/arc/$sympalistname
RedirectMatch ^/admin/$mmlistname                     https://$robot/wws/admin/$sympalistname
RedirectMatch ^/admindb/$mmlistname                   https://$robot/wws/modindex/$sympalistname

EOF
fi
